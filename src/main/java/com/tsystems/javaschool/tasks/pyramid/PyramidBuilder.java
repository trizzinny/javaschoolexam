package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        try {
            if (inputNumbers.contains(null) || inputNumbers.size() < 3)
                throw new CannotBuildPyramidException();

            int height = 1;
            int width = 1;
            int tmpSize = 1;

            while (tmpSize < inputNumbers.size()) {
                height++;
                width += 2;
                tmpSize += height;
            }

            if (inputNumbers.size() != tmpSize)
                throw new CannotBuildPyramidException();

            Collections.sort(inputNumbers);

            int[][] outArray = new int[height][width];
            int startPosition = width - 1;
            for (int i = height - 1; i >= 0; i--) {
                int count = i + 1;
                for (int j = startPosition; j >= 0; j -= 2) {
                    if (count > 0) {
                        outArray[i][j] = inputNumbers.get(tmpSize - 1);
                        tmpSize--;
                        count--;
                    }
                }
                startPosition--;
            }
            return outArray;
        } catch (OutOfMemoryError error) {
            throw new CannotBuildPyramidException();
        }
    }

}
